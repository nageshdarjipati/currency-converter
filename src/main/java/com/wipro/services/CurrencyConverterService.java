package com.wipro.services;

import java.util.Optional;

import io.swagger.model.ExchangeRequest;
import io.swagger.model.ExchangeResponse;


public interface CurrencyConverterService {
		Optional<ExchangeResponse> convert(ExchangeRequest request);
}
