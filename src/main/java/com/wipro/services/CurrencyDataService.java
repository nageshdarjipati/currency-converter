package com.wipro.services;

import java.math.BigDecimal;

import io.swagger.model.Currency;

public interface CurrencyDataService {
	
	BigDecimal getUnitExchangeRate(Currency source, Currency target);
}
