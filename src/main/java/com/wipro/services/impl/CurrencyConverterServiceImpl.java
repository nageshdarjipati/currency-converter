package com.wipro.services.impl;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.services.CurrencyConverterService;
import com.wipro.services.CurrencyDataService;

import io.swagger.model.ExchangeRequest;
import io.swagger.model.ExchangeResponse;

@Service
public class CurrencyConverterServiceImpl implements CurrencyConverterService {
	
	@Autowired
    private CurrencyDataService dataService;
	
	public CurrencyConverterServiceImpl(CurrencyDataService dataService) {
		this.dataService = dataService;
		
	}
    
	@Override
	public Optional<ExchangeResponse> convert(ExchangeRequest request) {

		ExchangeResponse response = null;
		if (valid(request)) {
			BigDecimal exchangeRate = dataService.getUnitExchangeRate(request.getSourceCurrencyCode(),request.getTargetCurrencyCode());
			response = mapResponse(request);
			response.exchangeRate(exchangeRate.setScale(2, BigDecimal.ROUND_HALF_UP));
			response.setExchangedAmount(request.getExchangeAmount().multiply(exchangeRate).setScale(2, BigDecimal.ROUND_HALF_UP));
		}
		return Optional.ofNullable(response);
	}

	/**
	 * This methods maps request parameters to response
	 * 
	 * @param request
	 * @return response
	 */
	private ExchangeResponse mapResponse(ExchangeRequest request) {
		ExchangeResponse response = new ExchangeResponse();
		response.setSourceCurrencyCode(request.getSourceCurrencyCode());
		response.setTargetCurrencyCode(request.getTargetCurrencyCode());
		return response;
	}

	/**
	 * This methods maps request parameters to response
	 * 
	 * @param request
	 * @return boolean
	 */
	private boolean valid(ExchangeRequest request) {

		return (request != null && request.getSourceCurrencyCode() != null && request.getTargetCurrencyCode() != null
				&& request.getExchangeAmount() != null && request.getExchangeAmount().doubleValue() >= 0);
	}

}