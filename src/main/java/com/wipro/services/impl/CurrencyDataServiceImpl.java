package com.wipro.services.impl;

import java.math.BigDecimal;
import java.util.EnumMap;

import org.springframework.stereotype.Service;

import com.wipro.services.CurrencyDataService;

import io.swagger.model.Currency;
@Service
public class CurrencyDataServiceImpl implements CurrencyDataService {

	EnumMap<Currency, Double> currencyMap = new EnumMap<>(Currency.class);
	public CurrencyDataServiceImpl() {
		loadExchangeRates();
	}
	
	@Override
	public BigDecimal getUnitExchangeRate(Currency source, Currency target) {
		double exchangeRate =  currencyMap.get(target)/currencyMap.get(source);
		return BigDecimal.valueOf(exchangeRate);
	}

	/**
	 * load ExchangeRates.
	 */
	private void loadExchangeRates() {
		currencyMap.put(Currency.EUR, 1.0);
		currencyMap.put(Currency.CHF, 1.11);
		currencyMap.put(Currency.GBP, 0.90);
		currencyMap.put(Currency.JPY, 1000.01);
		currencyMap.put(Currency.USD, 1.20);
	}

}
