package com.wipro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import com.wipro.configuration.SwaggerDocumentationConfig;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackages = { "io.swagger","com.wipro" })
@Import({
    SwaggerDocumentationConfig.class
})
public class CurrencyConverterApplication  {

  
    public static void main(String[] args) {
        new SpringApplication(CurrencyConverterApplication.class).run(args);
    }
}