package com.wipro.exceptions;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

public class CurrencyConversionHandler extends ResponseEntityExceptionHandler {

	  @ExceptionHandler(Exception.class)
	  protected ResponseEntity<Object> handleAllExceptions(Exception ex) {
	    return ResponseEntity.unprocessableEntity()
	        .header("com.wipro.error", ex.getMessage()).build();
	  }
	}
