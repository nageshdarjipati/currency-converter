package com.wipro.api.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.services.CurrencyConverterService;

import io.swagger.annotations.ApiParam;
import io.swagger.api.CurrenciesApi;
import io.swagger.model.ExchangeRequest;
import io.swagger.model.ExchangeResponse;

@RestController
public class CurrencyConveterApiController implements CurrenciesApi {
	
    @Autowired
    protected CurrencyConverterService currencyConveter;
    
    @Override
    public ResponseEntity<ExchangeResponse> exchangeMoney(@ApiParam(value = "Exchange Request" ,required=true )  @Valid @RequestBody ExchangeRequest body) {
    	Optional<ExchangeResponse> response = currencyConveter.convert(body);
    	if (response.isPresent()) {
    	      return ResponseEntity.ok(response.get());
    	}
    	return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

}
