package com.wipro.exceptions;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.wipro.exceptions.CurrencyConversionHandler;

public class CurrencyConversionHandlerTest {
	
	CurrencyConversionHandler handler = new CurrencyConversionHandler();

	@Before
	public void setup() {
	}

	@Test
	public void handleAllExceptionSuccess() {
		RuntimeException ex = new RuntimeException("Unknown Errorss");
		ResponseEntity<Object> responseEntity = handler.handleAllExceptions(ex);
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
	}

}
