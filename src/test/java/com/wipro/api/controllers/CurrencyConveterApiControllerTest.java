package com.wipro.api.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import io.swagger.model.Currency;
import io.swagger.model.ExchangeRequest;
import io.swagger.model.ExchangeResponse;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CurrencyConveterApiControllerTest extends AbstractTest {
	
	private static final String SERVICE_URL = "/currencies/exchange";

	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void convertCUrrencyWhenReturnSuccess() throws Exception {
		ExchangeRequest request = new ExchangeRequest();
		request.exchangeAmount(BigDecimal.valueOf(100.00));
		request.setSourceCurrencyCode(Currency.GBP);
		request.setTargetCurrencyCode(Currency.USD);
		String inputJson = super.mapToJson(request);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(SERVICE_URL).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		ExchangeResponse response = super.mapFromJson(content, ExchangeResponse.class);
		assertNotNull(response);
	}
	
	@Test
	public void convertCurrencyWhenRequestIsNull() throws Exception {
		ExchangeRequest request =null;
		String inputJson = super.mapToJson(request);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(SERVICE_URL).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(400, status);
	}
	
	@Test
	public void convertCurrencyWhenRequestIsInValid() throws Exception {
		ExchangeRequest request = new ExchangeRequest();
		request.setExchangeAmount(BigDecimal.valueOf(0.0));
		String inputJson = super.mapToJson(request);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(SERVICE_URL).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(400, status);
	}

}
