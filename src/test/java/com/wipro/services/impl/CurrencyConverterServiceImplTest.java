package com.wipro.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import com.wipro.services.CurrencyDataService;
import com.wipro.services.impl.CurrencyConverterServiceImpl;

import io.swagger.model.Currency;
import io.swagger.model.ExchangeRequest;
import io.swagger.model.ExchangeResponse;

public class CurrencyConverterServiceImplTest {

	  
	  private CurrencyConverterServiceImpl service;
	  
	  @Before
	  public void setup() {
	
		service = new CurrencyConverterServiceImpl(new CurrencyDataServiceImpl()) ;
	  }

	  @Test
	  public void convertUSDToEUROWhenAmountIsValid() {
		  
		ExchangeRequest request = new ExchangeRequest();
		request.exchangeAmount(BigDecimal.valueOf(120.00));
		request.setSourceCurrencyCode(Currency.USD);
		request.setTargetCurrencyCode(Currency.EUR);
		Optional<ExchangeResponse> result = service.convert(request);
		assertEquals(BigDecimal.valueOf(100.00).setScale(2), result.get().getExchangedAmount());
	  }
	  
	  @Test
	  public void convertGBPtoUSDPoundsWhenAmountIsValid() {
		  
		ExchangeRequest request = new ExchangeRequest();
		request.exchangeAmount(BigDecimal.valueOf(100.00));
		request.setSourceCurrencyCode(Currency.GBP);
		request.setTargetCurrencyCode(Currency.USD);
		Optional<ExchangeResponse> result = service.convert(request);
		assertEquals(BigDecimal.valueOf(133.33).setScale(2), result.get().getExchangedAmount());
	  }
	  

	  @Test
	  public void convertEUROsToPoundsWhenRequestIsInvalid() {
		  
		ExchangeRequest request = null;
		Optional<ExchangeResponse> result= service.convert(request);
		assertFalse(result.isPresent());
		request = new ExchangeRequest();
		result= service.convert(request);
		assertFalse(result.isPresent());
		request.setSourceCurrencyCode(Currency.EUR);
		result= service.convert(request);
		assertFalse(result.isPresent());
		request.setSourceCurrencyCode(Currency.EUR);
		request.setTargetCurrencyCode(Currency.USD);
		result= service.convert(request);
		assertFalse(result.isPresent());
	  }
	  
	  @Test
	  public void convertEUROsToPoundsWhenAmountisZero() {
		  
		ExchangeRequest request = new ExchangeRequest();
		request.exchangeAmount(BigDecimal.valueOf(0.00));
		request.setSourceCurrencyCode(Currency.GBP);
		request.setTargetCurrencyCode(Currency.USD);
		Optional<ExchangeResponse> result = service.convert(request);
		assertEquals(BigDecimal.valueOf(0.00).setScale(2), result.get().getExchangedAmount());
	  }

}
